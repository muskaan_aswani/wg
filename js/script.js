$(function () {
new WOW().init();

    $("#work-wrapper").magnificPopup({
        delegate:'a',
        type:'image',
        gallery:{
            enabled:true
        },
        zoom:{
            enabled:true,
            duration:300,
            easing: 'ease-in-out',
            opener:function(openerElement){
                return openerElement.is('img') ? openerElement :
                openerElement.find('img');
            }
        }
    });



  $("#team-members").owlCarousel({
      items:3,
      margin:30,
      loop:true,
      autoplay:true,
      smartSpeed:700,
      autoplayHoverPause:true
  });


  $("#test-members").owlCarousel({
      items:1,
      loop:true,
      autoplay:true,
      smartSpeed:700,
      autoplayHoverPause:true
  });


  $("#clients-slider").owlCarousel({
      items:6,
      margin:10,
      loop:true,
      autoplay:true,
      smartSpeed:700,
      autoplayHoverPause:true
  });


   $('.counter').counterUp({
      delay:10,
       time:1000
   }); 

});
